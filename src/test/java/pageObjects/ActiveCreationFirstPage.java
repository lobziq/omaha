package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertTrue;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class ActiveCreationFirstPage extends BasePage {
    @FindBy(name = "orgName")
    WebElement orgInput;

    public ActiveCreationFirstPage(WebDriver driver) {
        super(driver);
        name = "Создание актива (первый этап)";
    }

    @Override
    public void init() {
        super.init();
        assertTrue(!orgInput.isEnabled());
    }
}
