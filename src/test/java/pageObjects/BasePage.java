package pageObjects;

import jdk.nashorn.internal.ir.WhileNode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.*;
import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 10.02.2018.
 */
public class BasePage {
    public String name;
    public MainTableElement mainTableElement = null;

    public BasePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
    }

    public void init() {
        //
    }

    public boolean isElementPresent(WebElement e, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), timeout);
                wait.until(ExpectedConditions.visibilityOf(e));

            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean clickWithTimeout(WebElement e, int timeout) {
        double passedTime = 0;
        while (passedTime < timeout) {
            try {
                e.click();
                return true;
            } catch (Exception exception) {
                passedTime += 0.01;
            }
        }
        return false;
    }

    public void enterData(String placeholder, String data) {
        WebElement e = getDriver().findElement(By.xpath(String.format("//input[@placeholder='%s']", placeholder)));
        e.clear();
        e.sendKeys(data);
    }

    public void checkLogo(String text) {
        WebElement e = getDriver().findElement(By.xpath("//*[@title='" + text + "']"));
        isElementPresent(e, 10);
    }

    public void checkElement(String text) {
        WebElement e = getDriver().findElement(By.xpath("//*[text()='" + text + "']"));
        isElementPresent(e, 2);
        assertEquals(text, e.getText());
    }

    public void pushButton(String buttonText) {
        List<WebElement> elements = getDriver().findElements(By.xpath(String.format("//*[text()='%s']", buttonText, buttonText)));

        for (WebElement e: elements) {
            isElementPresent(e, 10);
            if (e.isDisplayed()) {
                e.click();
                return;
            }
        }
    }

    public void pushButtonInModalWindow(String modalWindowText, String buttonText) {
        WebElement modalWindow = getDriver().findElement(By.xpath(String.format("//*[text()='%s']/following-sibling::div",
                modalWindowText)));
        for (WebElement e: modalWindow.findElements(By.xpath(".//*"))) {
            if (e.getAttribute("innerText").contains(buttonText)) e.click();
            return;
        }
    }

    public void choseOption(String option) {
        WebElement e = getDriver().findElement(By.xpath(String.format("//option[text()='%s']", option)));
        isElementPresent(e, 30);
        e.click();;
    }
}
