package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static stepDefinitions.InitSteps.currentPage;
import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class ActiveCreationSecondPage extends BasePage {
    @FindBy(className = "breadcrumb")
    WebElement breadcrumb;

    @FindBy(xpath = "//*[@aria-describedby = 'rub-addon1']")
    WebElement basePriceInput;

    @FindBy(xpath = "//*[@aria-describedby = 'rub-addon2']")
    WebElement currentPriceInput;

    @FindBy(xpath = "//*[@aria-describedby = 'balanceInitialCurStatus']")
    WebElement balanceInitialCurStatus;

    @FindBy(xpath = "//*[@aria-describedby = 'balanceCurStatus']")
    WebElement balanceCurStatus;

    @FindBy(xpath = "//*[@aria-describedby = 'accountNumStatus']")
    WebElement accountNumStatus;

    @FindBy(xpath = "//*[@aria-describedby = 'startDateStatus']")
    WebElement startDateStatus;

    @FindBy(xpath = "//*[@aria-describedby = 'endDateStatus']")
    WebElement endDateStatus;

    @FindBy(id = "assetFormGroup")
    WebElement assetFormGroup;

    @FindBy(id = "myTab")
    WebElement tabList;

    Map<String, WebElement> inputBinding;

    public ActiveCreationSecondPage(WebDriver driver) {
        super(driver);
        name = "Создание актива (второй этап)";
    }

    @Override
    public void init() {
        super.init();
        checkTabs();

        inputBinding = new HashMap<>();
        inputBinding.put("Начальная базовая стоимость", basePriceInput);
        inputBinding.put("Текущая балансовая стоимость", currentPriceInput);
        inputBinding.put("Начальная базовая стоимость в валюте", balanceInitialCurStatus);
        inputBinding.put("Текущая балансовая стоимость в валюте", balanceCurStatus);
        inputBinding.put("Номер счёта", accountNumStatus);
        inputBinding.put("Дата начала действия актива", startDateStatus);
        inputBinding.put("Дата окончания действия актива", endDateStatus);
    }

    private void checkTabs() {
        List<String> expectedTabs = new ArrayList<>();
        String operations = "Основная информация,Приём,План работы,Судебная работа,Инвентаризация,Хранение,Аренда,Оценка,Реализация,Списание,Утилизация,Связи с активами,Вовлечённые стороны";
        expectedTabs.addAll(Arrays.asList(operations.split(",")));

        assertTrue(expectedTabs.stream().filter(s -> {
            for (WebElement tab: tabList.findElements(By.tagName("li"))) {
                if (tab.getAttribute("innerText").contains(s)) return false;
            }
            return true;
        }).count() == 0);
    }

    public void swithTabTo(String tabName) {
        for (WebElement tab: tabList.findElements(By.tagName("li"))) {
            if (tab.getAttribute("innerText").contains(tabName)) {
                tab.click();
                return;
            }
        }
    }

    public void fillInput(String name, String value) {
        breadcrumb.click();
        isElementPresent(inputBinding.get(name), 10);
        inputBinding.get(name).clear();
        inputBinding.get(name).sendKeys(value);
        breadcrumb.click();
    }

    public void clickOn(String name) {
        clickWithTimeout(inputBinding.get(name), 10);
    }

    public void compareInputs(String name, String name2) {
        //remove selection
        breadcrumb.click();
        inputBinding.get(name).getText().equals(inputBinding.get(name2).getText());
    }

    public void checkSelectedValueOption(String compare) {
        Select currencyStatus = new Select(getDriver().findElement(By.xpath("//*[@aria-describedby = 'currencyStatus']")));
        String selected = currencyStatus.getFirstSelectedOption().getText();
        assertTrue(selected.equals(compare));
    }

    public void pushBottomButtons(String text) {
        WebElement btn = assetFormGroup.findElement(By.xpath(String.format("//button[text()='%s']", text)));
        isElementPresent(btn, 10);
        btn.click();
    }
}
