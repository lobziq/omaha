package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertTrue;

import java.util.List;

import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class AccountPlanPage extends BasePage {
    @FindBy(id = "accountplancogrid_toppager_left")
    WebElement pager;

    public AccountPlanPage(WebDriver driver) {
        super(driver);
        name = "План счетов по КО";
    }

    @Override
    public void init() {
        super.init();
        mainTableElement = new MainTableElement(getDriver());
    }
}
