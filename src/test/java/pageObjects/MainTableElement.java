package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 12.02.2018.
 */
public class MainTableElement extends BasePage {
    public MainTableElement(WebDriver driver) {
        super(driver);
        isElementPresent(getDriver().findElements(By.xpath("//span[contains(@class, 'ui-icon fa fa-search')]")).get(0), 30);
    }

    public void selectAll() {
        WebElement allCheckbox = getDriver().findElement(By.id("assetgrid_checkAll"));
        isElementPresent(allCheckbox, 10);
        clickWithTimeout(allCheckbox, 10);
    }

    public void searchClick() {
        clickWithTimeout(getDriver().findElements(By.xpath("//span[contains(@class, 'ui-icon fa fa-search')]")).get(0), 30);
    }

    public void setSearch(String param1, String param2, String value) {
        choseOption(param1);
        choseOption(param2);
        getDriver().findElement(By.id("jqg2")).sendKeys(value);
    }

    public void compareStartsWith(String value, String mask) {
        int index = getIndexByHeader(value);
        WebElement table = getDriver().findElement(By.id("accountplancogrid"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        rows.remove(0);
        for (WebElement row: rows) {
            assertTrue(row.findElements(By.tagName("td")).get(index).getText().startsWith(mask));
        }
    }

    public int getIndexByHeader(String header) {
        WebElement tableHeader = getDriver().findElement(By.xpath("//*[@id=\"gview_accountplancogrid\"]/div[3]/div/table/thead"));
        List<WebElement> cells = tableHeader.findElements(By.tagName("th"));
        for (int i = 0; i < cells.size(); i++) {
            String text = cells.get(i).getAttribute("innerText");
            if (text.contains(header)) return i;
        }
        return 0;
    }
}
