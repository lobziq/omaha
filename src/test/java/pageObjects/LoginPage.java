package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import stepDefinitions.InitSteps.*;
import static org.junit.Assert.assertTrue;

import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 10.02.2018.
 */
public class LoginPage extends BasePage {
    @FindBy(className = "form-signin")
    WebElement loginForm;

    public LoginPage(WebDriver driver) {
        super(driver);
        name = "Вход в систему";
    }

    @Override
    public void init() {
        super.init();
        assertTrue(getDriver().findElement(By.xpath(String.format("//*[text()='%s']", name))) != null);
    }

    public void fillInput(String placeholder, String value) throws Throwable {
        try {
            loginForm.findElement(By.xpath(String.format("//input[@placeholder='%s']", placeholder))).sendKeys(value);
        } catch (Exception e) {
            throw e;
        }
    }

    public void fillCredentials(String name, String password) throws Throwable {
        enterData("Имя пользователя", name);
        enterData("Пароль", password);
    }

}
