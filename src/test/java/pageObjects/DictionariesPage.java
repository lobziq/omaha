package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class DictionariesPage extends BasePage {
    String tableHeadingClassSelector = "ui-jqgrid-htable";
    String tableBodyIdSelector = "dictionarylistgrid";

    public DictionariesPage(WebDriver driver) {
        super(driver);
        name = "Справочники";
    }

    public int getIndexByHeader(String header) {
        //getDriver().findElement(By.className(tableHeadingClassSelector)).getText();
        return 0;
    }

    public void clickOnDictionary(String dict) {
        List<WebElement> rows = getDriver().findElement(By.id(tableBodyIdSelector)).findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> links = row.findElements(By.tagName("a"));
            if (links.size() > 0 && links.get(0).getText().equalsIgnoreCase(dict)) {
                links.get(0).click();
                return;
            }
        }
    }
}
