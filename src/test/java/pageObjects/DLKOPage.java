package pageObjects;

import org.openqa.selenium.WebDriver;

import static stepDefinitions.InitSteps.getDriver;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class DLKOPage extends BasePage {
    public DLKOPage(WebDriver driver) {
        super(driver);
        name = "Документы ДЛКО";
    }

    @Override
    public void init() {
        super.init();
        mainTableElement = new MainTableElement(getDriver());
    }
}
