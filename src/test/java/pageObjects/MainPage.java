package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by a.shitov on 11.02.2018.
 */
public class MainPage extends BasePage {
    @FindBy(className = "navbar-default")
    WebElement navbar;

    public MainPage(WebDriver driver) {
        super(driver);
        name = "Главная страница";
    }

    public void selectFromMainMenu(String menuTitle, String menuItem) throws Throwable {
        List<WebElement> menuItems = navbar.findElements(By.className("dropdown"));
        WebElement navbarItem = menuItems.stream().filter(e -> e.getText().equalsIgnoreCase(menuTitle)).findFirst().get();
        navbarItem.click();
        WebElement item = navbarItem.findElement(By.xpath(String.format("//a[text()='%s']", menuItem)));
        clickWithTimeout(item, 5);
    }
}
