package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by a.shitov on 10.02.2018.
 */

@RunWith(Cucumber.class)

@CucumberOptions(features = {"src/test/java/features"},
        glue = "stepDefinitions",
        tags = {"@20"}
)

public class TestRunner {
}