package stepDefinitions;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageObjects.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by a.shitov on 10.02.2018.
 */
public class InitSteps {
    private static WebDriver driver = null;
    public static List<BasePage> pages = new ArrayList<>();
    public static BasePage currentPage;
    public static Map<String, String> testData = new HashMap<>();

    public static void setCurrentPage(String name) {
        currentPage = pages.stream().filter(p -> p.name.equals(name)).findFirst().get();
        currentPage.init();
    }

    public static WebDriver getDriver() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("-incognito");
            options.addArguments("dom.webnotifications.enabled");
            options.addArguments("-start-maximized");

            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            pages.add(new LoginPage(driver));
            pages.add(new MainPage(driver));
            pages.add(new DictionariesPage(driver));
            pages.add(new AccountPlanPage(driver));
            pages.add(new ActivesPage(driver));
            pages.add(new ActiveCreationFirstPage(driver));
            pages.add(new ActiveCreationSecondPage(driver));
            pages.add(new DLKOPage(driver));
            //loginPage = new LoginPage(driver);
        }
        return driver;
    }

    @Дано("^открыт браузер и осуществлен переход по ссылке \"([^\"]*)\"$")
    public void openPage(String url) throws Throwable {
        getDriver().get(url);
    }

    @И("^браузер закрыт$")
    public void closeBrowser() throws Throwable {
        getDriver().quit();
        driver = null;
    }
}
