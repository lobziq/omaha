package stepDefinitions;

import cucumber.api.DataTable;

import cucumber.api.PendingException;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.То;
import cucumber.api.java.ru.Тогда;
import org.openqa.selenium.By;
import pageObjects.*;


import java.util.List;
import static org.junit.Assert.assertTrue;
import static stepDefinitions.InitSteps.*;

/**
 * Created by a.shitov on 10.02.2018.
 */
public class StepImplementation {
    @И("^пользователь заполняет поле \"([^\"]*)\" значением \"([^\"]*)\"$")
    public void fillInput(String placeholder, String text) throws Throwable {
        currentPage.enterData(placeholder, text);
    }

    @И("^пользователь (нажимает кнопку|выбирает организацию) \"([^\"]*)\".*$")
    public void elementClick(String actions, String buttonText) throws Throwable {
        currentPage.pushButton(buttonText);
    }

    @И("^открывается страница \"([^\"]*)\"$")
    public void open(String windowText) throws Throwable {
        setCurrentPage(windowText);
    }

    @Когда("^пользователь заполняет поля \"Имя пользователя\" и \"Пароль\" значениями$")
    public void fillData(DataTable data) throws Throwable {
        List<String> dataList = data.asList(String.class);
        if (currentPage instanceof LoginPage) {
            ((LoginPage) currentPage).fillCredentials(dataList.get(0), dataList.get(1));
        }
    }

    @И("^открывается модальное окно \"([^\"]*)\"$")
    public void открываетсяМодальноеОкно(String window) throws Throwable {
        assertTrue(getDriver().findElement(By.xpath(String.format("//*[text()='%s']", window))) != null);
    }

    @Тогда("^появляется сообщение \"([^\"]*)\"$")
    public void checkMessage(String text) throws Throwable {
        currentPage.checkElement(text);
    }

    @Когда("^пользователь выбирает в меню \"([^\"]*)\" пункт \"([^\"]*)\"$")
    public void пользовательВыбираетВМенюПункт(String menu, String menuItem) throws Throwable {
        if (currentPage instanceof MainPage) {
            ((MainPage) currentPage).selectFromMainMenu(menu, menuItem);
        }
    }

    @Когда("^пользователь выбирает справочник \"([^\"]*)\" по \"([^\"]*)\"$")
    public void selectDictionary(String dict, String type) throws Throwable {
        if (currentPage instanceof DictionariesPage) {
            ((DictionariesPage) currentPage).clickOnDictionary(dict);
        }
    }

    @Когда("^пользователь начинает поиск$")
    public void пользовательНачинаетПоиск() throws Throwable {
        currentPage.mainTableElement.searchClick();
    }

    @И("^пользователь задает параметры \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void пользовательЗадаетПараметры(String param1, String param2, String value) throws Throwable {
        currentPage.mainTableElement.setSearch(param1, param2, value);
    }

    @Тогда("^проверяем что все \"([^\"]*)\" начинаются с \"([^\"]*)\"$")
    public void проверяемЧтоВсеНачинаютсяС(String value, String mask) throws Throwable {
        currentPage.mainTableElement.compareStartsWith(value, mask);
    }

    @Когда("^пользователь выбирает тип актива функциональный \"([^\"]*)\"$")
    public void пользовательВыбираетТипАктиваФункциональный(String option) throws Throwable {
        currentPage.choseOption(option);
    }

    @Когда("^пользователь вводит в поле \"([^\"]*)\" значение \"([^\"]*)\"$")
    public void вводитВПолеЗначение(String name, String value) throws Throwable {
        if (currentPage instanceof ActiveCreationSecondPage) {
            ((ActiveCreationSecondPage) currentPage).fillInput(name, value);
        }
    }

    @И("^пользователь нажимает на поле \"([^\"]*)\"$")
    public void пользовательНажимаетНаПоле(String arg0) throws Throwable {
        if (currentPage instanceof ActiveCreationSecondPage) {
            ((ActiveCreationSecondPage) currentPage).clickOn(arg0);
        }
    }

    @Тогда("^сравнить значение \"([^\"]*)\" и \"([^\"]*)\"$")
    public void сравнитьЗначениеИ(String arg0, String arg1) throws Throwable {
        if (currentPage instanceof ActiveCreationSecondPage) {
            ((ActiveCreationSecondPage) currentPage).compareInputs(arg0, arg1);
        }
    }

    @То("^проверить значение валюты \"([^\"]*)\"$")
    public void проверитьЗначениеВалюты(String arg0) throws Throwable {
        if (currentPage instanceof ActiveCreationSecondPage) {
            ((ActiveCreationSecondPage) currentPage).checkSelectedValueOption(arg0);
        }
    }

    @Когда("^пользователь выбирает все значения в таблице$")
    public void пользовательВыбираетВсеЗначенияВТаблице() throws Throwable {
        currentPage.mainTableElement.selectAll();
    }

    @И("^пользователь нажимает кнопку в модальном окне \"([^\"]*)\" \"([^\"]*)\"$")
    public void пользовательНажимаетКнопкуВМодальномОкне(String arg0, String arg1) throws Throwable {
        currentPage.pushButtonInModalWindow(arg0, arg1);
    }

    @Когда("^пользователь переходит на вкладку \"([^\"]*)\"$")
    public void пользовательПереходитНаВкладку(String arg0) throws Throwable {
        if (currentPage instanceof ActiveCreationSecondPage) {
            ((ActiveCreationSecondPage) currentPage).swithTabTo(arg0);
        }
    }
}
